#include "../tracers/TracerXML.h"
#include "../Controller.h"

namespace cdevs {
/**
 * \brief Destructor
 */
TracerXml::~TracerXml()
{
}

/**
 * \brief Starts the tracer
 *
 * @param recover  Flag determining whether the simulation is a recovered one
 */
void TracerXml::StartTracer(bool recover)
{
	Tracer::StartTracer(recover);
	if (!recover)
		PrintString("<trace>");
}

/**
 * \brief Stops the tracer
 */
void TracerXml::StopTracer()
{
	PrintString("</trace>");
	Tracer::StopTracer();
}

/**
 * \brief Performs the initialize trace for the given
 * atomic model at the given time
 *
 * @param aDevs  The model to perform the initialize trace for
 * @param t  The time of calling the initialize trace
 */
void TracerXml::TraceInitialize(std::shared_ptr<AtomicDevs> aDEVS, DevsTime t)
{
	std::string text = "\n";
	text += "<init>\n";
	text += "<model>" + aDEVS->get_model_full_name() + "</model>\n";
	text += "<time>" + t.string() + "</time>\n";
	text += "<kind>EX</kind>\n";
	text += "<state>\n" + aDEVS->getState().toXML() + "</state>\n";
	text += "<![CDATA[" + aDEVS->getState().string() + "]]>\n";
	text += "</init>\n";
	controller_->RunTrace(shared_from_this(), aDEVS, text);
}

/**
 * \brief Performs the internal trace for the given
 * atomic model
 *
 * @param devs  The model to perform the internal trace for
 */
void TracerXml::TraceInternal(std::shared_ptr<AtomicDevs> aDEVS)
{
	std::string text = "";
	text += "<kind>IN</kind>\n";
	for (auto port_msgs_pair : aDEVS->my_output_) {
		text += "<port name=\"" + port_msgs_pair.first.lock()->get_port_name() + "\" category=\"O\">\n";
		for (auto msg : port_msgs_pair.second) {
			if (msg.get())
				text += "<message>" + msg->string() + "</message>\n";
		}
		text += "</port>\n";
	}
	text += "<state>\n" + aDEVS->getState().toXML() + "</state>\n";
	text += "<![CDATA[" + aDEVS->getState().string() + "]]>\n";
	RunEventTraceAtController(aDEVS, text);
}

/**
 * \brief Performs the external trace for the given
 * atomic model
 *
 * @param aDevs  The model to perform the external trace for
 */
void TracerXml::TraceExternal(std::shared_ptr<AtomicDevs> aDEVS)
{
	std::string text = "";
	text += "<kind>EX</kind>\n";
	for (auto port_msgs_pair : aDEVS->my_input_) {
		text += "<port name=\"" + port_msgs_pair.first.lock()->get_port_name() + "\" category=\"I\">\n";
		for (auto msg : port_msgs_pair.second) {
			if (msg.get())
				text += "<message>" + msg->string() + "</message>\n";
		}
		text += "</port>\n";
	}
	text += "<state>\n" + aDEVS->getState().toXML() + "</state>\n";
	text += "<![CDATA[" + aDEVS->getState().string() + "]]>\n";
	RunEventTraceAtController(aDEVS, text);
}

/**
 * \brief Performs the confluent trace for the given
 * atomic model
 *
 * @param aDevs  The model to perform the confluent trace for
 */
void TracerXml::TraceConfluent(std::shared_ptr<AtomicDevs> aDEVS)
{
	std::string text = "";
	text += "<kind>IN</kind>\n";
	for (auto port_msgs_pair : aDEVS->my_output_) {
		text += "<port name=\"" + port_msgs_pair.first.lock()->get_port_name() + "\" category=\"O\">\n";
		for (auto msg : port_msgs_pair.second) {
			text += "<message>" + msg->string() + "</message>\n";
		}
		text += "</port>\n";
	}
	text += "<state>\n" + aDEVS->getState().toXML() + "</state>\n";
	text += "<![CDATA[" + aDEVS->getState().string() + "]]>\n";

	text += "<kind>EX</kind>\n";
	for (auto port_msgs_pair : aDEVS->my_input_) {
		text += "<port name=\"" + port_msgs_pair.first.lock()->get_port_name() + "\" category=\"I\">\n";
		for (auto msg : port_msgs_pair.second) {
			if (msg.get())
				text += "<message>" + msg->string() + "</message>\n";
		}
		text += "</port>\n";
	}
	text += "<state>\n" + aDEVS->getState().toXML() + "</state>\n";
	text += "<![CDATA[" + aDEVS->getState().string() + "]]>\n";
	RunEventTraceAtController(aDEVS, text);
}

/**
 * \brief Calls the controller to run the trace when it's appriopriate
 *
 * @param model  The model for which to run the trace
 * @param text  The text to output
 */
void TracerXml::RunEventTraceAtController(std::shared_ptr<AtomicDevs> model, std::string text)
{
	std::string text1 = "\n";
	text1 += "<event>\n";
	text1 += "<model>" + model->get_model_full_name() + "</model>\n";
	text1 += "<time>" + model->get_time_last().string() + "</time>\n";
	text1 += text;
	text1 += "</event>\n";

	controller_->RunTrace(shared_from_this(), model, text1);
}

/**
 * \brief Performs a trace for the given
 * atomic model
 *
 * @param model  The model to perform the trace for
 * @param time  The time at which the trace is invoked
 * @param text  The text to output
 */
void TracerXml::Trace(std::shared_ptr<AtomicDevs> model, DevsTime time, std::string text)
{
	PrintString(text);
}
}

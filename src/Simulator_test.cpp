#include "gtest/gtest.h"
#include "Simulator.h"
#include "trafficlight_parallel/model.h"

namespace cdevs {

class SimulatorTest: public ::testing::Test
{
protected:
	virtual void SetUp()
	{
		model = cdevs_examples::parallel::TrafficSystem::create("trafficSystem");

		sim = std::unique_ptr<Simulator>(new Simulator(model, 2));

		sim->set_termination_time(400.0);

		sim->Simulate();
	}
	virtual void TearDown()
	{
	}

	std::shared_ptr<cdevs_examples::parallel::TrafficSystem> model;
	std::unique_ptr<Simulator> sim;
};

TEST_F(SimulatorTest, Parallel)
{
	ASSERT_TRUE(model->getTrafficLight()->getState().getValue().compare("green") == 0);
}

}

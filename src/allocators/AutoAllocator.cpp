#include "AutoAllocator.h"

namespace cdevs {
/**
 * \brief Calculate allocations for the nodes, using the information provided.
 *
 * @param models The models to allocte
 * @param nr_nodes The number of nodes to allocate over. Simply an upper bound!
 * @return  allocation that was found
 */
void AutoAllocator::Allocate(std::list<std::shared_ptr<AtomicDevs>> models, int nr_nodes)
{
	//std::cout<< nr_nodes<<std::endl;
	int k = 0;
	for(auto m: models){
		if(m->get_location() == -1){
			//Model's location is the default, allowing the AutoAllocator to schedule it.
			//Provide an equal distribution of all models.
			//This does not keep in mind LP Load
			m->set_location(k, true);
			k++;
			k = k % nr_nodes;
		}
	}
}

/**
 * \brief Constructor
 */
AutoAllocator::AutoAllocator()
{
}

/**
 * \brief Returns the time it takes for the allocator to make an 'educated guess' of the advised allocation.
 * This time will not be used exactly, but as soon as the GVT passes over it. While this is not exactly
 * necessary, it avoids the overhead of putting such a test in frequently used code.
 *
 * @return float -- the time at which to perform the allocations (and save them)
 */
int AutoAllocator::GetTerminationTime()
{
	return 0;
}

/**
 * \brief Destructor
 */
AutoAllocator::~AutoAllocator()
{
}
}

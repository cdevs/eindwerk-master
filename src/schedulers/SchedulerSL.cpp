/*
 * SchedulerSL.cpp
 *
 *  Created on: 26-feb.-2015
 *      Author: david
 */

#include "SchedulerSL.h"

namespace cdevs {

/*
 * \brief Additional comparison method to compare DEVS models, based on their time_next_ as we need
 * this to return ImminentChildren.
 *
 * @param p1: Model to compare
 * @param p2: Model to compare
 *
 * @return bool: p1 < p2 to sort the List.
 */
bool Compare(std::shared_ptr<AtomicDevs> p1, std::shared_ptr<AtomicDevs> p2)
{

	return p1->get_time_next() < p2->get_time_next();
}

/*
 * \brief Creates the Scheduler, assigning all models and parameters.
 *
 * @param models: List of models for the Scheduler.
 * @param epsilon: Time to wait between current time for imminent.
 * @param totalModels: Number of models.
 */
SchedulerSL::SchedulerSL(std::list<std::shared_ptr<AtomicDevs>> &models, float epsilon, unsigned int totalModels)
	: models_(models), epsilon_(epsilon), totalmodels_(totalModels)
{
}

/*
 * \brief Default constructor for the Scheduler.
 */
SchedulerSL::SchedulerSL()
	: models_(), epsilon_(), totalmodels_()
{
}

/*
 * \brief Schedules a model.
 * @param model: Model to be scheduled.
 */
void SchedulerSL::Schedule(std::shared_ptr<AtomicDevs> model)
{
	auto p = std::find(models_.begin(), models_.end(), model);
	if (p == models_.end()) {
		this->models_.push_back(model);
		this->models_.sort(Compare);
	}

}

/*
 * \brief Unschedules the model, removes it from the scheduler if it is found, otherwise ignored.
 * @param model: Model to unschedule, does nothing if the model is not found.
 */
void SchedulerSL::Unschedule(std::shared_ptr<AtomicDevs> model)
{
	std::list<std::shared_ptr<AtomicDevs> >::iterator i = models_.begin();
	while (i != models_.end()) {
		if (*i == model) {
			i = models_.erase(i); //reseat iterator
		} else {
			++i;
		}
	}
	//No need to sort again, as removing doesn't change ordering.
}
/*
 * \brief Gets all models from the reschedule set, unschedules them if they are already present
 * in the models_ of the scheduler, and schedules them all.
 *
 * @param reschedule_set: List of all the models to be rescheduled
 */
void SchedulerSL::MassReschedule(std::list<std::shared_ptr<AtomicDevs>> &reschedule_set)
{
	for (auto m : reschedule_set) {
		Unschedule(m);
		Schedule(m);
	}
}

/*
 * \brief Returns the first model, more precisely, that with the lowest time next.
 *
 * @return DevsTime: Devstime of the model with the lowest time next.
 */
DevsTime SchedulerSL::ReadFirst()
{
	return this->models_.front()->get_time_next();
}
/*
 * \brief Used to get all imminent models for the next transition given a time DevsTime.
 * @param time: Time before which transitions that want to fire get added to imminent
 *
 * @return Returns all models that want to fire before time.
 */
std::list<std::shared_ptr<AtomicDevs> > SchedulerSL::GetImminent(DevsTime time)
{
	std::list<std::shared_ptr<AtomicDevs> > b;
	for (auto iterator = models_.begin(); iterator != models_.end(); ++iterator) {
		if ((std::fabs((*iterator)->get_time_next().get_x() - time.get_x()) < this->epsilon_)
		        && ((*iterator)->get_time_next().get_y() == time.get_y())) {
			b.push_back(*iterator);
		}
	}
	return b;
}

SchedulerSL::~SchedulerSL()
{
}

/**
 * \brief Gets the epsilon value of the scheduler
 *
 * @return  The epsilon value of the scheduler
 */
double SchedulerSL::get_epsilon()
{
	return this->epsilon_;
}

/**
 * \brief Gets the total amount of models of the scheduler
 *
 * @return  The total amount of models of the scheduler
 */
int SchedulerSL::get_total_models()
{
	return this->totalmodels_;
}

} /* namespace ns_DEVS */

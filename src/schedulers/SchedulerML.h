/*
 * SchedulerML.h
 *
 *  Created on: 26-feb.-2015
 *      Author: david
 *      Author: Nathan
 */

#ifndef SCHEDULERML_H_
#define SCHEDULERML_H_

#include "../templates/Scheduler.h"
#include "../AtomicDevs.h"
#include <list>
#include "../utility.h"
#include <cfloat>
#include <cmath>
#include <algorithm>

#include <cereal/access.hpp>
#include <cereal/types/base_class.hpp>
#include <cereal/types/memory.hpp>
#include <cereal/types/list.hpp>
#include <cereal/types/polymorphic.hpp>

namespace cdevs {

/**
 * The Minimal List scheduler is the simplest scheduler available, though it
 * has extremely bad performance in most cases.
 *
 * It simply keeps a list of all models. As soon as a reschedule happens,
 * the list is checked for the minimal value, which is stored. When the imminent
 * models are requested, it iterates the complete list in search of models that qualify.
 */
class SchedulerML: public Scheduler
{
public:
	SchedulerML(std::list<std::shared_ptr<AtomicDevs> > models, float epsilon, unsigned int totalModels);
	virtual ~SchedulerML();
	void Schedule(std::shared_ptr<AtomicDevs> model);
	void Unschedule(std::shared_ptr<AtomicDevs> model);
	void MassReschedule(std::list<std::shared_ptr<AtomicDevs> > &reschedule_set);
	DevsTime ReadFirst();
	std::list<std::shared_ptr<AtomicDevs> > GetImminent(DevsTime time);
	double get_epsilon() const;
	void set_epsilon(double epsilon);
	int get_total_models() const;
	void set_total_models(int totalmodels);

private:
	SchedulerML();

	friend class cereal::access;

	template<class Archive>
	void serialize(Archive & ar)
	{
		ar(cereal::base_class<Scheduler>(this), models_, minval_, epsilon_, totalmodels_);
	}

	std::list<std::shared_ptr<AtomicDevs> > models_;
	DevsTime minval_;
	double epsilon_;
	int totalmodels_;

};

} /* namespace ns_DEVS */

#include <cereal/archives/binary.hpp>

CEREAL_REGISTER_TYPE(cdevs::SchedulerML)

#endif /* SCHEDULERML_H_ */

/*
 * LogicDevsException.h
 *
 *  Created on: May 5, 2015
 *      Author: paul
 */

#ifndef LOGICDEVSEXCEPTION_H_
#define LOGICDEVSEXCEPTION_H_

#include <stdexcept>

namespace cdevs {

class LogicDevsException: public std::logic_error
{
public:
	using std::logic_error::logic_error;
};

} /* namespace cdevs */

#endif /* LOGICDEVSEXCEPTION_H_ */

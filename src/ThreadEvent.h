#ifndef SRC_THREADEVENT_H_
#define SRC_THREADEVENT_H_

#include <mutex>
#include <condition_variable>

namespace cdevs {

/**
 * A class that provides the python threading.Event object.
 * An event object manages an internal flag that can be set to true with the Set() method
 * and reset to false with the Clear() method. The Wait() method blocks until the flag is true.
 */

class ThreadEvent
{
public:
	ThreadEvent();
	virtual ~ThreadEvent();

	const bool isSet() const;

	void Wait(int time_out = 0);
	void Set();
	void Clear();
private:
	bool flag_;
	std::mutex mutex_;
	std::condition_variable cv_;
};

} /* namespace cdevs */

#endif /* SRC_THREADEVENT_H_ */

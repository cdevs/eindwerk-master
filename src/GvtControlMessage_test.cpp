#include "GvtControlMessage.h"
#include "gtest/gtest.h"
#include <map>

namespace cdevs
{
TEST(GvtControlMessage_Test, DefaultConstructor)
{
	GvtControlMessage gvt_control_message;

	EXPECT_EQ(gvt_control_message.get_clock(), 0.0);
	EXPECT_EQ(gvt_control_message.get_send(), 0.0);
	EXPECT_TRUE(gvt_control_message.get_wait_vector().size() == 0);
	EXPECT_TRUE(gvt_control_message.get_accumulate_vector().size() == 0);
}

TEST(GvtControlMessage_Test, ParameterConstructor)
{
	std::map<unsigned, int> test_wait_vector = { {0, 5}, {7, 2} };
	std::map<unsigned, int> test_accumulate_vector = { {5, 2}, {9, 2} };

	GvtControlMessage gvt_control_message(5.0, 4.0, test_wait_vector, test_accumulate_vector);

	EXPECT_EQ(gvt_control_message.get_clock(), 5.0);
	EXPECT_EQ(gvt_control_message.get_send(), 4.0);

	EXPECT_TRUE(gvt_control_message.get_wait_vector().at(0) == 5);
	EXPECT_TRUE(gvt_control_message.get_wait_vector().at(7) == 2);

	EXPECT_TRUE(gvt_control_message.get_accumulate_vector().at(5) == 2);
	EXPECT_TRUE(gvt_control_message.get_accumulate_vector().at(9) == 2);

	gvt_control_message = GvtControlMessage(std::numeric_limits<double>::infinity(), std::numeric_limits<double>::epsilon(), test_wait_vector, test_accumulate_vector);

	EXPECT_EQ(gvt_control_message.get_clock(), std::numeric_limits<double>::infinity());
	EXPECT_EQ(gvt_control_message.get_send(), std::numeric_limits<double>::epsilon());

	EXPECT_TRUE(gvt_control_message.get_wait_vector().at(0) == 5);
	EXPECT_TRUE(gvt_control_message.get_wait_vector().at(7) == 2);

	EXPECT_TRUE(gvt_control_message.get_accumulate_vector().at(5) == 2);
	EXPECT_TRUE(gvt_control_message.get_accumulate_vector().at(9) == 2);
}

TEST(GvtControlMessage_Test, SettersGetters)
{
	GvtControlMessage gvt_control_message;

	// clock
	gvt_control_message.set_clock(5.0);
	EXPECT_EQ(gvt_control_message.get_clock(), 5.0);

	gvt_control_message.set_clock(0.0);
	EXPECT_EQ(gvt_control_message.get_clock(), 0.0);

	gvt_control_message.set_clock(std::numeric_limits<double>::infinity());
	EXPECT_EQ(gvt_control_message.get_clock(), std::numeric_limits<double>::infinity());

	gvt_control_message.set_clock(std::numeric_limits<double>::epsilon());
	EXPECT_EQ(gvt_control_message.get_clock(), std::numeric_limits<double>::epsilon());

	// send
	gvt_control_message.set_send(5.0);
	EXPECT_EQ(gvt_control_message.get_send(), 5.0);

	gvt_control_message.set_send(0.0);
	EXPECT_EQ(gvt_control_message.get_send(), 0.0);

	gvt_control_message.set_send(std::numeric_limits<double>::infinity());
	EXPECT_EQ(gvt_control_message.get_send(), std::numeric_limits<double>::infinity());

	gvt_control_message.set_send(std::numeric_limits<double>::epsilon());
	EXPECT_EQ(gvt_control_message.get_send(), std::numeric_limits<double>::epsilon());

	std::map<unsigned, int> test_vector = { {0, 5}, {7, 2} };

	// wait vector
	gvt_control_message.set_accumulate_vector({});
	EXPECT_TRUE(gvt_control_message.get_accumulate_vector().size() == 0);

	gvt_control_message.set_accumulate_vector(test_vector);
	EXPECT_TRUE(gvt_control_message.get_accumulate_vector().at(0) == 5);
	EXPECT_TRUE(gvt_control_message.get_accumulate_vector().at(7) == 2);

	// wait vector
	gvt_control_message.set_accumulate_vector({});
	EXPECT_TRUE(gvt_control_message.get_accumulate_vector().size() == 0);

	gvt_control_message.set_accumulate_vector(test_vector);
	EXPECT_TRUE(gvt_control_message.get_accumulate_vector().at(0) == 5);
	EXPECT_TRUE(gvt_control_message.get_accumulate_vector().at(7) == 2);
}
}

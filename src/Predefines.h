/*************************************************************************
 *
 * $Id: predef.h,v 1.2 2005/05/22 11:50:58 breese Exp $
 *
 * Copyright (C) 2003 Bjorn Reese <breese@users.sourceforge.net>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND WITHOUT ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED WARRANTIES OF
 * MERCHANTIBILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE AUTHORS AND
 * CONTRIBUTORS ACCEPT NO RESPONSIBILITY IN ANY CONCEIVABLE MANNER.
 *
 ************************************************************************/

/*
 * References:
 *   http://predef.sourceforge.net/
 *   http://www.UNIX-systems.org/v3-apis.html
 *   http://petef.port5.com/c/portability.txt
 */

#ifndef SRC_PREDEFINES_H_
#define SRC_PREDEFINES_H_

/*************************************************************************
 * Compilers
 */

/*
 * Cygwin
 */
#if defined(__CYGWIN__)
# define PREDEF_COMPILER_CYGWIN 0
#endif

/*
 * GNU C/C++
 *
 * Version: VVRRPP : VV = Version, RR = Revision, PP = Patch
 * Example: 030200 = GCC 3.0.2
 */
#if defined(__GNUC__)
# if defined(__GNUC_PATCHLEVEL__)
#  define PREDEF_COMPILER_GCC ((__GNUC__ * 10000) + \
                               (__GNUC_MINOR__ * 100) + \
                               (__GNUC_PATCHLEVEL__))
# else
#  define PREDEF_COMPILER_GCC ((__GNUC__ * 10000) + \
                               (__GNUC_MINOR__ * 100))
# endif
#endif

/*
 * MinGW32
 */
#if defined(__MINGW32__)
# define PREDEF_COMPILER_MINGW32 0
#endif

/*
 * Microsoft Visual C++
 *
 * Version: VVRR : VV = Version, RR = Revision
 * Example: 1200 = Visual C++ 6.0 (compiler 12.0)
 */
#if defined(_MSC_VER)
# define PREDEF_COMPILER_VISUALC _MSC_VER
# define PREDEF_VERSION_VISUALC_6_0 1200
# define PREDEF_VERSION_VISUALC_7_0 1300
#endif

/*************************************************************************
 * Operating Systems
 */

#if defined(linux) \
 || defined(__linux) \
 || defined(__linux__)
# define PREDEF_OS_LINUX
#endif

#if defined(__MACOSX__) \
 || (defined(__APPLE__) && defined(__MACH__))
# define PREDEF_OS_MACOSX
#endif

/*************************************************************************
 * Platforms
 */

#if defined(WIN32) \
 || defined(_WIN32) \
 || defined(__TOS_WIN__) \
 || defined(PREDEF_COMPILER_VISUALC)
# define PREDEF_PLATFORM_WIN32
# define PREDEF_OS_WINDOWS
#endif

#endif /* SRC_PREDEFINES_H_ */

set(MODELNAME "TrafficLight_Parallel")

#=====================================================================#
# Sources                                                             #
#=====================================================================#
set(EXAMPLESOURCES
   ${EXAMPLESOURCES}
   ${CMAKE_CURRENT_SOURCE_DIR}/model.cpp
   CACHE INTERNAL "example sources"
)

#=====================================================================#
# Executables                                                         #
#=====================================================================#
# All executables and their linkage should be done here

add_library(${MODELNAME} SHARED model.cpp export.cpp)
target_link_libraries(${MODELNAME} cdevs)

add_executable ("${MODELNAME}_experiment" experiment.cpp model.cpp)
target_link_libraries("${MODELNAME}_experiment" cdevs dl)

add_executable ("${MODELNAME}_resume" resume.cpp model.cpp)
target_link_libraries("${MODELNAME}_resume" cdevs dl)
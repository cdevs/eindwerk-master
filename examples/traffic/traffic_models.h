/*
 * traffic_models.h
 *
 *  Created on: 5-jun.-2015
 *      Author: david
 */

#ifndef EXAMPLES_TRAFFIC_MODELS_H_
#define EXAMPLES_TRAFFIC_MODELS_H_

#include "../../src/AtomicDevs.h"
#include "../../src/CoupledDEVS.h"
#include <random>
#include <array>

namespace cdevs_examples {
namespace traffic {

typedef std::list<std::shared_ptr<const cdevs::EventBase> > Outbag;
typedef std::map<std::weak_ptr<cdevs::Port>, Outbag, std::owner_less<std::weak_ptr<cdevs::Port> > > Outbags;

enum Events
{
	kCar = 0, kQuery, kQueryAck, Count
};

enum Direction
{
	kNorth = 0, kEast = 1, kSouth = 2, kWest = 3
};

// forward declarations
class City;
class Building;

class Car: public cdevs::EventCRTP<Car, kCar>
{
public:
	Car(unsigned ID, float v, int v_pref, int dv_pos_max, int dv_neg_max, float departure_time);
	std::string string() const;
	bool operator==(Car const& rhs) const;
private:
	unsigned ID_;
	int v_pref_;
	int dv_pos_max_;
	int dv_neg_max_;
	float departure_time_;
	float distance_travelled_;
	float remaining_x_;
	float v_;
	std::string path_;
};

class Query: public cdevs::EventCRTP<Query, kQuery>
{
public:
	Query(unsigned ID);
	std::string string() const;
	bool operator==(Query const& rhs) const;
private:
	unsigned ID_;
	char direction_;
};

class QueryAck: public cdevs::EventCRTP<QueryAck, kQueryAck>
{
	friend class Building;
public:
	QueryAck(unsigned ID, float t_until_dep);
	std::string string() const;
	bool operator==(QueryAck const& rhs) const;
private:
	unsigned ID_;
	float t_until_dep_;
};

class BuildingState: public cdevs::State<BuildingState>
{
	friend class Building;
public:
	BuildingState(int IAT_min, int IAT_max, std::string path, std::string name);
	std::string string() const;

	float Uniform(int min, int max);
private:
	int IAT_min_;
	int IAT_max_;
	std::string path_;
	std::string name_;
	float current_time_;
	float send_query_delay_;
	float send_car_delay_;
	unsigned send_query_id_;
	unsigned send_car_id_;
	int next_v_pref_;
	unsigned sent_;
	std::default_random_engine generator_;
};

class Building: public cdevs::Atomic<Building, BuildingState>
{
public:
	Building(unsigned district, std::string path = "", std::string name = "Building", int IAT_min = 100,
	        int IAT_max = 100, int v_pref_min = 15, int v_pref_max = 15, int dv_pos_max = 15, int dv_neg_max = 150);
	BuildingState const& IntTransition();
	Outbags OutputFunction();
	double TimeAdvance();
	BuildingState const& ExtTransition(Outbags inputs);
protected:
	// copy of arguments
	unsigned district_;
	std::string path_;
	std::string name_;
	int IAT_min_;
	int IAT_max_;
	int v_pref_min_;
	int v_pref_max_;
	int dv_pos_max_;
	int dv_neg_max_;

	// output ports
	std::weak_ptr<cdevs::Port> q_sans_;
	std::weak_ptr<cdevs::Port> q_send_;
	std::weak_ptr<cdevs::Port> exit_;

	// input ports
	std::weak_ptr<cdevs::Port> q_rans_;
	std::weak_ptr<cdevs::Port> entry_;

	// state
	unsigned send_max_;
};

//class Residence: public Building<Residence>
//{
//public:
//	Residence(std::string path, unsigned district, std::string name = "Residence", int IAT_min = 100, int IAT_max =
//	        100, int v_pref_min = 15, int v_pref_max = 15, int dv_pos_max = 15, int dv_neg_max = 15);
//};
//
//class CommercialState: public cdevs::State<CommercialState>
//{
//	friend class Commercial;
//public:
//	CommercialState();
//	CommercialState(Car car);
//	std::string string() const;
//private:
//	std::shared_ptr<Car> car_;
//};
//
//class Commercial: public Building<Commercial, CommercialState>
//{
//public:
//	Commercial(unsigned district, std::string name="Commercial");
//
//	CommercialState const& ExtTransition(Outbags inputs);
//	CommercialState const& IntTransition();
//	Outbags OutputFunction();
//	double TimeAdvance();
//private:
//	// output ports:
//	std::weak_ptr<cdevs::Port> to_collector_;
//};

class RoadSegmentState: public cdevs::State<RoadSegmentState>
{
	friend class RoadSegment;
public:
	RoadSegmentState();
	std::string string() const;
	bool operator==(RoadSegmentState const& rhs) const;
private:
	std::list<Car> cars_present_;
	std::list<Query> query_buffer_;
	std::list<Query> deny_list_;
	bool reserved_ = false;
	double send_query_delay_;
	int send_query_id_;
	double send_ack_delay_;
	int send_ack_id_;
	double send_car_delay_;
	int send_car_id_;
	int last_car_;
};

class RoadSegment: public cdevs::Atomic<RoadSegment, RoadSegmentState>
{
	friend class Road;
public:
	RoadSegment(unsigned district, std::string name = "RoadSegment", double l = 100, double v_max = 18,
	        double observ_delay = 0.1);
	RoadSegmentState const& ExtTransition(Outbags inputs);
	RoadSegmentState const& IntTransition();
	Outbags OutputFunction();
	double TimeAdvance();
	double MinTime();
private:
	double l_;
	double v_max_;
	double observ_delay_;
	unsigned district_;
	std::string name_;

	// input ports
	std::weak_ptr<cdevs::Port> q_rans_;
	std::weak_ptr<cdevs::Port> q_recv_;
	std::weak_ptr<cdevs::Port> car_in_;
	std::weak_ptr<cdevs::Port> entries_;
	std::weak_ptr<cdevs::Port> q_rans_bs_;
	std::weak_ptr<cdevs::Port> q_recv_bs_;

	// output ports
	std::weak_ptr<cdevs::Port> q_sans_;
	std::weak_ptr<cdevs::Port> q_send_;
	std::weak_ptr<cdevs::Port> car_out_;
	std::weak_ptr<cdevs::Port> exits_;
	std::weak_ptr<cdevs::Port> q_sans_bs_;
};

class Road: public cdevs::Coupled<Road>
{
public:
	Road(unsigned district, std::string name = "Road", unsigned segments = 5);
private:
	unsigned district_;
	unsigned segments_;

	std::vector<std::shared_ptr<RoadSegment> > segment_;

	// input ports
	std::weak_ptr<cdevs::Port> q_rans_;
	std::weak_ptr<cdevs::Port> q_recv_;
	std::weak_ptr<cdevs::Port> car_in_;
	std::weak_ptr<cdevs::Port> entries_;
	std::weak_ptr<cdevs::Port> q_rans_bs_;
	std::weak_ptr<cdevs::Port> q_recv_bs_;

	// output ports
	std::weak_ptr<cdevs::Port> q_sans_;
	std::weak_ptr<cdevs::Port> q_send_;
	std::weak_ptr<cdevs::Port> car_out_;
	std::weak_ptr<cdevs::Port> exits_;
	std::weak_ptr<cdevs::Port> q_sans_bs_;
};

class IntersectionState: public cdevs::State<IntersectionState>
{
public:
	IntersectionState(double switch_signal);
	std::string string() const;
private:
	double switch_signal_;

	std::list<Query> send_query_;
	std::list<QueryAck> send_ack_;
	std::list<Car> send_car_;
	std::list<Query> queued_queries_;

	std::array<bool, 4> block_;
	std::map<unsigned, int> id_locations_;
	std::map<int, unsigned> int_to_dir_;
};

class Intersection: public cdevs::Atomic<Intersection, IntersectionState>
{
public:
	Intersection(unsigned district, std::string name = "Intersection", double switch_signal = 30);
	IntersectionState const& ExtTransition(Outbags inputs);
	IntersectionState const& IntTransition();
	Outbags OutputFunction();
	double TimeAdvance();
private:
	unsigned district_;
	double switch_signal_;
	double switch_signal_delay_;

	std::array<std::weak_ptr<cdevs::Port>, 4> q_send_;
	std::array<std::weak_ptr<cdevs::Port>, 4> q_rans_;
	std::array<std::weak_ptr<cdevs::Port>, 4> q_recv_;
	std::array<std::weak_ptr<cdevs::Port>, 4> q_sans_;
	std::array<std::weak_ptr<cdevs::Port>, 4> car_in_;
	std::array<std::weak_ptr<cdevs::Port>, 4> car_out_;
};

class CollectorState: public cdevs::State<CollectorState>
{
	friend class Collector;
public:
	CollectorState();
	std::string string() const;
private:
	std::list<Car> cars_;
};

class Collector: public cdevs::Atomic<Collector, CollectorState>
{
	friend class City;
public:
	Collector();
	CollectorState const& ExtTransition(Outbags inputs);
private:
	std::weak_ptr<cdevs::Port> car_in_;
	unsigned district_;
};

}
}

#endif /* EXAMPLES_TRAFFIC_MODELS_H_ */
